<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bluestormtheme
 */

?>
			</div><!--/row-->
		</div><!--/container-->
	</div><!-- #content -->
<?php if (!is_page_template('page-landing.php')): ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-widgets">
			<div class="container">
				<div class="row">
					<div class="footer-left col-sm-6">
						<?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
								<?php dynamic_sidebar( 'sidebar-2' ); ?>
						<?php } ?>
					</div>
					<div class="footer-right col-sm-6">
						<?php if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
								<?php dynamic_sidebar( 'sidebar-3' ); ?>
						<?php } ?>
					</div>
				</div><!--/row-->
			</div><!--/container-->
		</div>

		<div class="container">
			<div class="row">
				<div class="site-info">
					<nav class="footer-menu"><?php wp_nav_menu( array('menu' => 'Footer Menu' )); ?></nav>
					<p class="copyright">Copyright &copy; <?php echo date('Y'); ?> <?php echo bloginfo('name'); ?> | <a href="https://bluestormcreative.com" target="blank">Website Development</a> | <a href="/privacy-policy/">Privacy</a> | <a href="/terms-of-use/">Terms</a></p>
				</div><!-- .site-info -->
			</div><!--/row-->
		</div><!--/container-->
	</footer><!-- #colophon -->
<?php endif; // end of check for landing page ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
