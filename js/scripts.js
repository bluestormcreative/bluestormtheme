jQuery(document).ready(function( $ ) {   //Wrap functions to prevent issues with no conflict mode

//Clears form default values on focus.
var el = $('input[type=text], input[type=email], textarea');
    el.focus(function(e) {
        if (e.target.value == e.target.defaultValue)
            e.target.value = '';
    });
    el.blur(function(e) {
        if (e.target.value == '')
            e.target.value = e.target.defaultValue;
    });



});
