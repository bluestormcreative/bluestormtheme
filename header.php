<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bluestormtheme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'bluestormtheme' ); ?></a>

<?php if (!is_page_template('page-landing.php')): ?>
	<header id="masthead" class="site-header" role="banner">

		<div class="container">
			<div class="row">
				<div class="site-branding col-sm-4">
					<?php
					if ( get_field('logo', 'option') ) :

						$image = get_field('logo', 'option');
						?>
						<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
					<?php else : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
					endif;
					?>
				</div><!-- .site-branding -->

				<nav id="site-navigation" class="main-navigation col-sm-8" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
				</nav><!-- #site-navigation -->
			</div><!--/row-->
		</div> <!-- /container -->

	</header><!-- #masthead -->
<?php endif; // end of check for landing page ?>

<?php if (is_front_page() && get_field('banner_image', 'option')):
		$banner = get_field('banner_image', 'option');
	?>
	<div class="banner" style="background-image:url('<?php echo $banner['url']; ?>');">
		<div class="container">
			<div class="row row-equal-height">
				<div class="banner-video col-sm-6">
					<?php if (get_field('banner_video_embed_code', 'option')): ?>
					<!-- <a href="#" class="video-button"></a> -->
					<div class="video-code" style="display:none;">
						<?php the_field('banner_video_embed_code', 'option'); ?>
					</div>
					<?php endif; ?>
				</div>
				<div class="banner-optin col-sm-6">
					<?php if (get_field('banner_optin_text', 'option')): ?>
						<div class="banner-text">
							<?php the_field('banner_optin_text', 'option'); ?>
						</div>
						<div class="banner-code">
							<?php the_field('banner_optin_code', 'option'); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

	<div id="content" class="site-content">
		<div class="container">
			<div class="row">
