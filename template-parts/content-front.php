<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rachelcall
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->

		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'rachelcall' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'rachelcall' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->

	</div><!-- .entry-content -->
</article><!-- #post-## -->

<div class="front-posts">
	<h2 class="section-heading">Recent Posts</h2>
	<div class="row row-eq-height">
		<?php
				$query = new WP_Query('posts_per_page=4');
				if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
				<div class="col-xs-12 col-sm-3">
					<a class="front-post" href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail(); ?>
						<div class="front-post-title"><?php the_title(); ?></div>
					</a>
				</div>
		  <?php endwhile; ?>
		<?php endif; wp_reset_postdata(); ?>
	</div><!--/row-->
</div><!--/front-posts-->
