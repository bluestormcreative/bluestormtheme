
#bluestormtheme

###Current Version: 1.0.1
===

Custom starter theme built on Bootstrap and _s by Blue Storm Creative.

* Bootstrap 3.3.6
* _s as of 3/22/2016

**Plugin Dependencies:**

* Advanced Custom Fields Pro
* Wordpress Responsive Menu




---------------
