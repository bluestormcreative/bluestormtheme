<?php
/**
 * Template Name: Full Width (no sidebar)
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bluestormtheme
 */

get_header(); ?>

	<div id="primary" class="content-area col-sm-12">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
